
// Exercise 2
// Faisel Al

#include <iostream>
#include <conio.h>

using namespace std;

enum Suit
{
	Spade, Heart, Diamond, Clubs
};

enum Rank
{
	Two = 2, Three, Four, Five, Six, Seven, Eight, Nine, Ten, Jack, Queen, King, Ace
};

struct Card
{
	Rank rank;
	Suit suit;
};
void PrintCard(Card card)
{
	std::cout << "The ";

	switch (card.rank)
	{
	case Two: std::cout << "two"; break;
	case Three: std::cout << "three"; break;
	case Four: std::cout << "four"; break;
	case Five: std::cout << "five"; break;
	case Six: std::cout << "six"; break;
	case Seven: std::cout << "seven"; break;
	case Eight: std::cout << "eight"; break;
	case Nine: std::cout << "nine"; break;
	case Ten: std::cout << "ten"; break;
	case Jack: std::cout << "jack"; break;
	case Queen: std::cout << "queen"; break;
	case King: std::cout << "king"; break;
	case Ace: std::cout << "ace"; break;
	}
	cout << " of ";
	switch (card.suit)
	{
	case Spade: std::cout << "spade \n"; break;
	case Heart: std::cout << "heart \n"; break;
	case Diamond: std::cout << "diamond \n"; break;
	case Clubs: std::cout << "clubs \n"; break;

	}
		
}
Card HighCard(Card c1, Card c2)
{
	if (c1.rank >= c2.rank) return c1;
	return c2;
}

int main()
{
	Card c;
	c.rank = Nine;
	c.suit = Clubs;

	Card c2;
	c2.rank = Ten;
	c2.suit = Heart;

	PrintCard(c);
	_getch();
	return 0;
}
